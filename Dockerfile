FROM python:3.9-buster

RUN apt-get update -y && apt-get upgrade -y

COPY . /app

RUN apt-get install -y curl ffmpeg texlive-pictures texlive-science texlive-latex-extra latexmk \
    && mkdir /apps \
    && cd /apps \
	# europarl tools are used to prepare subtitles for Gargantua
    && mkdir europarl \
    && curl -SL http://www.statmt.org/europarl/v7/tools.tgz | tar -xzC europarl

WORKDIR /app

RUN pip install -r requirements.txt

RUN useradd -ms /bin/bash corpus_user
USER corpus_user

CMD ["python", "run.py"]