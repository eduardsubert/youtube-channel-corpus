from typing import Any, List, Optional, Tuple
import orjson
import asyncio
import aiofiles


def save_json(filename: str, content: Any) -> None:
    with open(filename, "w") as f:
        f.write(orjson.dumps(content).decode())


async def async_save_json(filename: str, content: Any) -> None:
    async with aiofiles.open(filename, mode="w") as f:
        await f.write(orjson.dumps(content).decode())


def load_json(filename: str) -> Any:
    try:
        with open(filename, "r") as f:
            return orjson.loads(f.read())
    except (ValueError, FileNotFoundError):
        return None


async def async_load_json(filename: str) -> Any:
    try:
        async with aiofiles.open(filename, mode="r") as f:
            return orjson.loads(await f.read())
    except (ValueError, FileNotFoundError):
        return None


async def run_cmd_async(cmd: List[str], input: Optional[str] = None) -> Tuple[str, int]:
    stdin = asyncio.subprocess.PIPE if input is not None else None
    process = await asyncio.create_subprocess_exec(*cmd, stdin=stdin, stdout=asyncio.subprocess.PIPE)

    if input is not None:
        process.stdin.write(input.encode())
        process.stdin.write_eof()
        await process.stdin.drain()

    data = await process.stdout.read()
    code = await process.wait()
    return data.decode().strip(), code
