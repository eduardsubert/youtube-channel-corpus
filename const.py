import os

logging_level = os.environ["LOGGING_LEVEL"]
author = os.environ["AUTHOR"]

entity_unrecognized_words_ratio_limit = float(os.environ["ENTITY_UNRECOGNIZED_WORDS_RATIO_LIMIT"])
sentence_unrecognized_words_ratio_limit = float(os.environ["SENTENCE_UNRECOGNIZED_WORDS_RATIO_LIMIT"])

data_dir = os.environ["DATA_DIR"]

subtitles_extension = os.environ["SUBTITLE_EXTENSION"]
videos_language = os.environ["VIDEOS_LANGUAGE"]
channel_id = os.environ["CHANNEL_ID"]
files_entity_dir = os.environ["FILES_ENTITY_DIR"]

dir_tmpl = os.path.join(data_dir, files_entity_dir)
metadata_file = os.path.join(data_dir, os.environ["FILES_METADATA"])
report_file = os.path.join(data_dir, os.environ["FILES_REPORT"])
corpus_file_tmpl = os.path.join(data_dir, os.environ["FILES_CORPUS"])
audio_segment_file_tmpl = os.path.join(data_dir, os.environ["FILES_AUDIO_SEGMENT"])
entity_metadata_file_tmpl = os.path.join(dir_tmpl, os.environ["FILES_ENTITY_METADATA"])
processed_text_file_tmpl = os.path.join(dir_tmpl, os.environ["FILES_PROCESSED_TEXT"])
text_alignment_file_tmpl = os.path.join(dir_tmpl, os.environ["FILES_TEXT_ALIGNMENT"])
audio_alignment_file_tmpl = os.path.join(dir_tmpl, os.environ["FILES_AUDIO_ALIGNMENT"])
audio_file_tmpl = os.path.join(dir_tmpl, os.environ["FILES_AUDIO"])
triplets_file_tmpl = os.path.join(dir_tmpl, os.environ["FILES_TRIPLETS"])
processing_info_file_tmpl = os.path.join(dir_tmpl, os.environ["FILES_PROCESSING_INFO"])


cache_metadata = os.environ["CACHE_METADATA"].lower() in ["true", "1"]
cache_download = os.environ["CACHE_DOWNLOAD"].lower() in ["true", "1"]
cache_text_processing = os.environ["CACHE_TEXT_PROCESSING"].lower() in ["true", "1"]
cache_text_alignment = os.environ["CACHE_TEXT_ALIGNMENT"].lower() in ["true", "1"]
cache_audio_alignment = os.environ["CACHE_AUDIO_ALIGNMENT"].lower() in ["true", "1"]
cache_triplets = os.environ["CACHE_TRIPLETS"].lower() in ["true", "1"]
cache_audio_segments = os.environ["CACHE_AUDIO_SEGMENTS"].lower() in ["true", "1"]
