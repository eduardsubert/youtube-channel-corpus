from itertools import chain
from const import videos_language, sentence_unrecognized_words_ratio_limit


def score_audio_alignment(audio_alignment):
    unrecognized_words = [word for word in audio_alignment["words"] if word["case"] != "success"]
    return len(unrecognized_words) / len(audio_alignment["words"])


def create_filtered_triplets(audio_alignment, text_alignment):
    result = {}
    for language, text_alignment_for_language in text_alignment.items():
        if text_alignment_for_language is None:
            continue
        language_result = []
        found_alignment_sentences = find_alignment_sentences(audio_alignment)

        for sources_language_sentence, target_language_sentence, pairing in zip(
            text_alignment_for_language["source_language"],
            text_alignment_for_language["target_language"],
            text_alignment_for_language["pairing"],
        ):
            source_sentence_words = list(
                chain.from_iterable(
                    found_alignment_sentences[int(sentence_index) - 1][1] for sentence_index in pairing[0].split(" ")
                )
            )
            successful_source_sentence_words = [word for word in source_sentence_words if word["case"] == "success"]

            sentence_unrecognized_words_ratio = 1 - len(successful_source_sentence_words) / len(source_sentence_words)
            if sentence_unrecognized_words_ratio >= sentence_unrecognized_words_ratio_limit:
                continue

            language_result.append(
                {
                    "source_language": sources_language_sentence,
                    "target_language": target_language_sentence,
                    "start": successful_source_sentence_words[0]["start"],
                    "end": successful_source_sentence_words[-1]["end"],
                    "sentence_unrecognized_words_ratio": sentence_unrecognized_words_ratio,
                }
            )
        result[language] = language_result
    return result


def find_alignment_sentences(audio_alignment):
    result = []
    words = audio_alignment["words"]
    sentences = [sentence.strip() for sentence in audio_alignment["transcript"].split("\n")]

    words_index = 0
    for sentence in sentences:
        start_words_index = words_index

        while words_index < len(words) and words[words_index]["word"] in sentence:
            end_words_index = words_index
            words_index += 1

        result.append((sentence, words[start_words_index : end_words_index + 1]))
    return result
