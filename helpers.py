import logging
import os
from io import BytesIO
from typing import Any, Dict, Tuple

import aiofiles
import httpx
import pyvtt
import youtube_dl
import asyncio
from ffmpeg import FFmpeg


from common import run_cmd_async
from const import dir_tmpl, subtitles_extension


def get_youtube_ids_for_channel(channel_id: str) -> Dict[str, Any]:
    options = {
        "logger": logging.getLogger(__name__),
        "simulate": True,
        "no_warnings": True,
    }
    with youtube_dl.YoutubeDL(options) as ydl:
        return ydl.extract_info(f"https://www.youtube.com/user/{channel_id}/videos")


async def download_youtube_audio_and_subs(entity_id: str) -> Dict[str, Any]:
    async with httpx.AsyncClient() as client:
        response = await client.post("http://youtubedl/", json={"youtube_id": entity_id}, timeout=None)
        if response.status_code == 200:
            return response.json()


async def load_subtitles(entity_id: str) -> Dict[str, Dict[str, str]]:
    subtitles = {}
    with os.scandir(dir_tmpl % {"id": entity_id}) as it:
        for entry in it:
            if entry.name.startswith(".") or not entry.is_file():
                continue
            _, extension = os.path.splitext(entry.name)
            if extension != "." + subtitles_extension:
                continue

            language = entry.name.split(".")[-2].split("-")[0]
            async with aiofiles.open(entry.path, mode="r") as f:
                subtitles[language] = {"raw": await f.read()}
    return subtitles


async def process_subtitles(subtitles_string: str, language: str) -> Tuple[str, str, str]:
    subtitles = pyvtt.from_string(subtitles_string)
    subtitles.clean_text(tags=True, brackets=True, keys=True, trailing=True)
    text = " ".join(caption.text for caption in subtitles)

    split_sentences, split_sentences_code = await run_cmd_async(
        ["/apps/europarl/tools/split-sentences.perl", "-q", "-l", language], text
    )
    tokenized, tokenized_code = await run_cmd_async(
        ["/apps/europarl/tools/tokenizer.perl", "-q", "-l", language], split_sentences
    )

    return text, split_sentences, tokenized


async def align_text(
    source_tokenized: str, source_untokenized: str, target_tokenized: str, target_untokenized: str
) -> Dict[str, str]:
    async with httpx.AsyncClient() as client:
        data = {
            "source_tokenized": source_tokenized,
            "source_untokenized": source_untokenized,
            "target_tokenized": target_tokenized,
            "target_untokenized": target_untokenized,
        }
        response = await client.post("http://gargantua/", json=data, timeout=None)
        if response.status_code == 200:
            return response.json()


async def align_audio(audio_path: str, transcription: str) -> Dict[str, str]:
    async with httpx.AsyncClient() as client:
        files = {
            "audio": open(audio_path, "rb"),
            "transcript": BytesIO(bytes(transcription, encoding="utf-8")),
        }
        response = await client.post("http://gentle:8765/transcriptions?async=false", files=files, timeout=None)
        if response.status_code == 200:
            return response.json()


async def chop_audio(audio_path: str, times: Dict[str, Tuple[float, float]]) -> None:
    for output_audio_path, time in times.items():
        ffmpeg = (
            FFmpeg().option("y").option("ss", time[0]).option("to", time[1]).input(audio_path).output(output_audio_path)
        )

        await ffmpeg.execute()
