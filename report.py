from pylatex import Document, Section, Subsection, Table, Command, Tabular, Itemize
from pylatex.utils import NoEscape, bold
from const import (
    report_file,
    author,
    channel_id,
    entity_unrecognized_words_ratio_limit,
    sentence_unrecognized_words_ratio_limit,
)


def build_report(split_corpus):
    doc = Document("basic")

    doc.preamble.append(Command("title", f"YouTube Channel Corpus: {channel_id}"))
    doc.preamble.append(Command("author", author))
    doc.preamble.append(Command("date", NoEscape(r"\today")))
    doc.append(NoEscape(r"\maketitle"))

    with doc.create(Section("Copyright Statement")):
        doc.append(
            f"I, {author}, do solemnly swear that I have received written promission from the owner of the YouTube channel {channel_id} to use their content for the creation of this language corpus. "
        )

    with doc.create(Section("Corpus Creation Methodology")):
        doc.append(
            f"""Corpus was created from audio and manually created subtitles downloaded from YouTube channel {channel_id}. 
            Plain text transcription is split by the Europarl tools and aligned by sentence alignment tool Gargantua. 
            Next, video transcript in it's source language is aligned to it's audio by  Gentle, an English forced-aligner built on the Kaldi ASR toolkit. """
        )

        doc.append(f"""Then comes filtering based on the output of Gentle to discard potentially noisy data. """)

        with doc.create(Itemize()) as itemize:
            itemize.add_item(
                f"""Whole videos are removed if the number of unrecognized words is equal to or greater than {int(entity_unrecognized_words_ratio_limit * 100)}% of the total words in the video. """
            )
            itemize.add_item(
                f"""Single sentences are removed if the number of unrecognized words is equal to or greater than {int(sentence_unrecognized_words_ratio_limit * 100)}% of the total words in the sentence. """
            )

        doc.append(f"""Lastly, appropriate audio segments are created by ffmpeg without re-encoding.""")

        # #Talks, #Sentences, Hours of speech, Source words, Target words
        entity_counts = {}
        sentences_counts = {}
        seconds_counts = {}
        source_words_counts = {}
        target_words_counts = {}

        for languages, _ in (entity_corpus for entity_corpus in split_corpus if entity_corpus is not None):
            for language, sentences in languages.items():
                entity_counts[language] = entity_counts.get(language, 0) + 1
                sentences_counts[language] = sentences_counts.get(language, 0) + len(sentences)
                seconds_counts[language] = seconds_counts.get(language, 0) + sum(
                    sentence["end"] - sentence["start"] for sentence in sentences
                )
                source_words_counts[language] = source_words_counts.get(language, 0) + len(
                    ("".join(sentence["source_language"] for sentence in sentences)).split(" ")
                )
                target_words_counts[language] = target_words_counts.get(language, 0) + len(
                    ("".join(sentence["target_language"] for sentence in sentences)).split(" ")
                )
        with doc.create(Table(position="h!")) as statistics_table:
            with doc.create(Tabular("l|r|r|r|r|r")) as statistics_tabular:
                statistics_tabular.add_row(
                    "",
                    "#Entities",
                    "#Sentences",
                    "Hours",
                    "Source words",
                    "Target words",
                    mapper=[bold],
                )
                statistics_tabular.add_hline()
                for language in entity_counts:
                    statistics_tabular.add_row(
                        language,
                        entity_counts[language],
                        sentences_counts[language],
                        "{:.2f}".format(seconds_counts[language] / 3600),
                        source_words_counts[language],
                        target_words_counts[language],
                    )
            statistics_table.add_caption("Basic statistics of the corpus. ")

    doc.generate_pdf(report_file, clean_tex=False)
