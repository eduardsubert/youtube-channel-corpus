import asyncio
from typing import List
import arrow
import logging
import os
import sys
from collections import defaultdict
from itertools import chain

from common import async_save_json, async_load_json, save_json, load_json
from const import (
    audio_alignment_file_tmpl,
    audio_file_tmpl,
    channel_id,
    metadata_file,
    entity_metadata_file_tmpl,
    processed_text_file_tmpl,
    text_alignment_file_tmpl,
    processing_info_file_tmpl,
    videos_language,
    logging_level,
    cache_metadata,
    cache_download,
    cache_text_processing,
    cache_text_alignment,
    cache_audio_alignment,
    cache_triplets,
    cache_audio_segments,
    author,
    entity_unrecognized_words_ratio_limit,
    triplets_file_tmpl,
    corpus_file_tmpl,
    audio_segment_file_tmpl,
)
from helpers import (
    align_audio,
    align_text,
    download_youtube_audio_and_subs,
    get_youtube_ids_for_channel,
    load_subtitles,
    process_subtitles,
    chop_audio,
)
from corpus import score_audio_alignment, create_filtered_triplets
from report import build_report

# LOGGERS
logging.basicConfig(
    level=logging_level,
    format="%(asctime)s %(message)s",
)
logger = logging.getLogger(__name__)


async def process_entity(entity_id: str) -> None:
    entity_processing_info = [arrow.now().format("YYYY-MM-DD HH:mm:ss ZZ")]
    logger.info("Starting %s", entity_id)
    # download
    audio_file = audio_file_tmpl % {"id": entity_id}
    entity_metadata_file = entity_metadata_file_tmpl % {"id": entity_id}
    if not cache_download or not os.path.isfile(audio_file):
        logger.debug("Downloading %s", entity_id)
        await async_save_json(entity_metadata_file, await download_youtube_audio_and_subs(entity_id))

    # europarl
    processed_text_file = processed_text_file_tmpl % {"id": entity_id}

    subtitles = None
    do_processing = False

    if cache_text_processing:
        subtitles = await async_load_json(processed_text_file)

    if subtitles is None:
        subtitles = await load_subtitles(entity_id)
        do_processing = True

    # such entities are not suitable for processing
    if videos_language not in subtitles:
        entity_processing_info.append("Not processed for lack of source language subtitles.")
        await async_save_json(processing_info_file_tmpl % {"id": entity_id}, entity_processing_info)
        logger.info(f"Not processing {entity_id}")
        return

    if do_processing:
        logger.debug("Text processing %s", entity_id)
        for language, language_subtitles in subtitles.items():
            (
                language_subtitles["text"],
                language_subtitles["untokenized"],
                language_subtitles["tokenized"],
            ) = await process_subtitles(language_subtitles["raw"], language)
        await async_save_json(processed_text_file, subtitles)

    # gargantua
    text_alignment_file = text_alignment_file_tmpl % {"id": entity_id}
    text_alignment = None
    if cache_text_alignment:
        text_alignment = await async_load_json(text_alignment_file)
    if text_alignment is None:
        logger.debug("Text alignment %s", entity_id)
        text_alignment = {
            language: await align_text(
                subtitles[videos_language]["tokenized"],
                subtitles[videos_language]["untokenized"],
                language_subtitles["tokenized"],
                language_subtitles["untokenized"],
            )
            for language, language_subtitles in subtitles.items()
            if language != videos_language
        }
        await async_save_json(text_alignment_file, text_alignment)

    # gentle
    audio_file = audio_file_tmpl % {"id": entity_id}
    audio_alignment_file = audio_alignment_file_tmpl % {"id": entity_id}
    audio_alignment = None
    if cache_audio_alignment:
        audio_alignment = await async_load_json(audio_alignment_file)
    if audio_alignment is None:
        logger.debug("Audio alignment %s", entity_id)
        audio_alignment = await align_audio(
            audio_path=audio_file,
            transcription=subtitles[videos_language]["tokenized"],
        )
        await async_save_json(audio_alignment_file, audio_alignment)

    # corpus
    if (
        entity_unrecognized_words_ratio := score_audio_alignment(audio_alignment)
    ) >= entity_unrecognized_words_ratio_limit:
        entity_processing_info.append(
            f"Discarded for too high unrecognized words ratio: {entity_unrecognized_words_ratio}"
        )
        await async_save_json(processing_info_file_tmpl % {"id": entity_id}, entity_processing_info)
        return

    triplets_file = triplets_file_tmpl % {"id": entity_id}
    triplets = None
    if cache_triplets:
        triplets = await async_load_json(triplets_file)
    if triplets is None:
        triplets = create_filtered_triplets(audio_alignment, text_alignment)
        await async_save_json(triplets_file, triplets)

    # chop audio
    for sentences in triplets.values():
        for sentence in sentences:
            audio_segment_file = audio_segment_file_tmpl % {
                "id": entity_id,
                "s": sentence["start"],
                "e": sentence["end"],
            }
            sentence["audio_file"] = audio_segment_file
            if not cache_audio_segments or not os.path.isfile(audio_alignment_file):
                await chop_audio(
                    audio_path=audio_file, times={audio_segment_file: (sentence["start"], sentence["end"])}
                )

    await async_save_json(processing_info_file_tmpl % {"id": entity_id}, entity_processing_info)
    logger.info("Finished %s", entity_id)
    return triplets, audio_file


async def main(entity_ids: List[str]) -> None:
    return await asyncio.gather(*[process_entity(entity_id) for entity_id in entity_ids])


if __name__ == "__main__":
    if not author:
        sys.exit(
            "Please state your name in the .env file as the AUTHOR variable. Make sure to acquire written promission to use any copyrighted material. Your name won't be sent anywhere. "
        )

    metadata = None
    if cache_metadata:
        metadata = load_json(metadata_file)

    if metadata is None:
        metadata = get_youtube_ids_for_channel(channel_id)
        save_json(metadata_file, metadata)

    entity_ids = [entry["id"] for entry in metadata["entries"]]
    split_corpus = asyncio.run(main(entity_ids))

    corpus = defaultdict(list)
    for triplets, audio_file in (entity_corpus for entity_corpus in split_corpus if entity_corpus is not None):
        for language, sentences in triplets.items():
            corpus[language].extend(
                (
                    # f"{audio_file} {sentence['start']} {sentence['end']}",
                    sentence["audio_file"],
                    sentence["source_language"],
                    sentence["target_language"],
                )
                for sentence in sentences
            )

    for language, language_corpus in corpus.items():
        with open(corpus_file_tmpl % {"language": language}, "w") as f:
            f.write("\n".join(chain.from_iterable(language_corpus)))

    # report
    build_report(split_corpus)
