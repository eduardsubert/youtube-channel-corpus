from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import os
import youtube_dl

app = FastAPI()


class InputData(BaseModel):
    youtube_id: str


options = {
    "outtmpl": os.path.join("/data", os.environ["FILES_ENTITY_DIR"], os.environ["FILES_AUDIO"]),
    "simulate": False,
    "no_warnings": True,
    "ignoreerrors": True,
    "format": "bestaudio/best",
    "noplaylist": True,
    "allsubtitles": True,
    "writesubtitles": True,
    "writeautomaticsub": False,
    "subtitlesformat": "vtt",
    "writeinfojson": True,
    "postprocessors": [
        {
            "key": "FFmpegExtractAudio",
            "preferredcodec": "aac",
            "preferredquality": "192",
        }
    ],
}


@app.post("/")
def download_audio_and_subtitles(data: InputData):
    try:
        with youtube_dl.YoutubeDL(options) as ydl:
            ydl.download([f"https://www.youtube.com/watch?v={data.youtube_id}"])
    except Exception as e:
        raise HTTPException(status_code=500, detail="Error during processing: " + str(e)) from e
